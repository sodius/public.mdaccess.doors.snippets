# MDAccess for DOORS Snippets

This repository holds resources for a developer to get started on capabilities provided by MDAccess for DOORS.

See [https://www.sodiuswillert.com](https://www.sodiuswillert.com)

## Preparing your Eclipse Platform

Eclipse is the recommended platform to execute the snippets.

Here are the necessary steps to have a working environment for the snippets: 

1. In Eclipse, click **Help > Install New Software...**
2. Use the **https://download.sodius.com/** location to install following features:
     * MDAccess for DOORS
4. Restart Eclipse once the features have been downloaded and installed, as suggested by Eclipse
5. In Eclipse, import the [com.sodius.mdw.metamodel.doors.snippets](https://bitbucket.org/sodius/public.mdaccess.doors.snippets/src/master/src/com.sodius.mdw.metamodel.doors.snippets/) project from the snippets repository.

At this stage the code should successfully compile in **com.sodius.mdw.metamodel.doors.snippets** project.

## Obtaining a License

MDAccess for DOORS is license protected. Contact us to get a SodiusWillert license file to run the snippets

## Executing Snippets

This reporistory contains snippets demonstrating how to use Java code to query and update resources in a DOORS database.

Those snippets are located here: [/src/com.sodius.mdw.metamodel.doors.snippets/src/com/sodius/mdw/metamodel/doors/snippets/](https://bitbucket.org/sodius/public.mdaccess.doors.snippets/src/master/src/com.sodius.mdw.metamodel.doors.snippets/src/com/sodius/mdw/metamodel/doors/snippets/)

The Javadoc of each snippet details the expected Java VM argument for the snippet to run properly.

Refer to the [Developer Guide](https://help.sodius.cloud/help/topic/com.sodius.mdw.metamodel.doors.doc/docs/overview.html)
of MDAccess for DOORS for details:

### Snippet List

Here are the snippets you may want to start with:

* [PrintRootFolders](https://bitbucket.org/sodius/public.mdaccess.doors.snippets/src/master/src/com.sodius.mdw.metamodel.doors.snippets/src/com/sodius/mdw/metamodel/doors/snippets/PrintRootFolders.java): prints in the console the root folders of a DOORS database.
* [PrintModuleContentSnippet](https://bitbucket.org/sodius/public.mdaccess.doors.snippets/src/master/src/com.sodius.mdw.metamodel.doors.snippets/src/com/sodius/mdw/metamodel/doors/snippets/PrintModuleContentSnippet.java): prints in the console the content of a DOORS module.
* [CreateFormalModuleSnippet](https://bitbucket.org/sodius/public.mdaccess.doors.snippets/src/master/src/com.sodius.mdw.metamodel.doors.snippets/src/com/sodius/mdw/metamodel/doors/snippets/CreateFormalModuleSnippet.java): creates a new DOORS module with some objects, pictures, tables, etc.
* [UpdateAttributeValueSnippet](https://bitbucket.org/sodius/public.mdaccess.doors.snippets/src/master/src/com.sodius.mdw.metamodel.doors.snippets/src/com/sodius/mdw/metamodel/doors/snippets/UpdateAttributeValueSnippet.java): updates the value of one specific DOORS Object attribute.

The snippets package contains more snippets, make sure to review all of them to see all capabilities being demonstrated.


### Connecting to a DOORS client application

MDAccess for DOORS uses a DoorsApplication instance to connect to DOORS and perform operations. 
A DoorsApplication instance is what is used to execute DXL code to retrieve information. 

If the Java System property "doors.isSilent" is not set or is false, an interactive application is going to be used.
This mean the Java program will connect to the active DOORS client through COM automation.The execution will fail if a DOORS client is not running at that time. 

If the System property "doors.isSilent" is set to true, a batch application is going to be used.
This means the Java program will trigger to startup of a DOORS client running in batch modeand will use it for the lifetime of the program execution.
The DOORS batch client is automatically shutdown when DoorsApplication.dispose() is called.
If batch mode is requested, there are additional System properties that need to be configured. 

System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab. 

Here are the required System properties to execute the snippet: 

* -Dmdw.license=<path> -The location of the MDAccess for DOORS license file. Either the path of the license file in your file system or the address of the floating server (e.g. @myserver). 

If a DOORS batch client is to use, here are additional required System properties: 

* -Ddoors.path="C:\Program Files\IBM\Rational\DOORS\9.7\bin\doors.exe"
* -Ddoors.portserver=36677@myServer
* -Ddoors.user=myUser
* -Ddoors.password=myPassword
