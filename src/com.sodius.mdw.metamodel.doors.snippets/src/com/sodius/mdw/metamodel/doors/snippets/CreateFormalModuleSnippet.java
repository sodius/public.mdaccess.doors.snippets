package com.sodius.mdw.metamodel.doors.snippets;

import java.io.IOException;
import java.io.InputStream;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.model.RichText;
import com.sodius.mdw.metamodel.doors.io.commands.Command;
import com.sodius.mdw.metamodel.doors.io.commands.CommandRunner;
import com.sodius.mdw.metamodel.doors.io.commands.CreateModuleCommand;
import com.sodius.mdw.metamodel.doors.io.commands.CreateObjectFragment;
import com.sodius.mdw.metamodel.doors.io.commands.CreatePictureFragment;
import com.sodius.mdw.metamodel.doors.io.commands.CreateTableFragment;
import com.sodius.mdw.metamodel.doors.io.commands.EditModuleCommand;
import com.sodius.mdw.metamodel.doors.io.commands.EditModuleCommand.TerminationMode;
import com.sodius.mdw.metamodel.doors.io.commands.InsertLocation;
import com.sodius.mdw.metamodel.doors.io.commands.ModuleRef;
import com.sodius.mdw.metamodel.doors.io.commands.ObjectRef;
import com.sodius.mdw.metamodel.doors.io.commands.PictureRef;
import com.sodius.mdw.metamodel.doors.io.commands.SetAttributeValueFragment;
import com.sodius.mdw.metamodel.doors.richtext.Attachment;
import com.sodius.mdw.metamodel.doors.richtext.AttachmentResolver;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Creates a new DOORS module with some objects, pictures, tables, etc.
 *
 * <p>
 * Here are the required Program arguments:
 * <ul>
 * <li><code>args[0]=&lt;moduleQualifiedName&gt;</code> - the qualified name of a DOORS module to create (e.g. "/ParentFolder/MyModule").
 * The parent folder must already exist
 * </li>
 * </ul>
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class CreateFormalModuleSnippet extends AbstractCommandSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expects a module qualified name as argument");
        }
        String moduleQualifiedName = args[0];

        new CreateFormalModuleSnippet(moduleQualifiedName).call();
    }

    private final String moduleQualifiedName;
    private final AttachmentResolver attachmentResolver;

    public CreateFormalModuleSnippet(String moduleQualifiedName) {
        this.moduleQualifiedName = moduleQualifiedName;
        this.attachmentResolver = new AttachmentResolverImpl();
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application, CommandRunner runner) throws Exception {
        System.out.println("Creating module: " + moduleQualifiedName);
        runner.run(createCommand());
    }

    private Command createCommand() {

        // create the module
        ModuleRef module = ModuleRef.qualifiedName(moduleQualifiedName);
        CreateModuleCommand command = new CreateModuleCommand(module, TerminationMode.SAVE_AND_CLOSE);

        // create objects
        ObjectRef rootObject = createHeadingObject(command);
        createObjectWithText(command, InsertLocation.below(rootObject));
        createObjectWithPicture(command, InsertLocation.below(rootObject));
        createObjectWithAttachment(command, InsertLocation.below(rootObject));
        createTable(command, InsertLocation.below(rootObject));

        return command;
    }

    private ObjectRef createHeadingObject(EditModuleCommand command) {
        CreateObjectFragment fragment = new CreateObjectFragment();
        command.add(fragment);
        ObjectRef object = ObjectRef.created(fragment);

        RichText text = RichText.valueOf("Introduction");
        command.add(new SetAttributeValueFragment(object, "Object Heading", text));

        return object;
    }

    private void createObjectWithText(EditModuleCommand command, InsertLocation location) {
        CreateObjectFragment fragment = new CreateObjectFragment(location);
        command.add(fragment);
        ObjectRef object = ObjectRef.created(fragment);

        RichText text = RichText.valueOf("<html><body><p>Some <b>formatted</b> <i>text</i></p></body></html>");
        command.add(new SetAttributeValueFragment(object, "Object Text", text));
    }

    private void createObjectWithAttachment(EditModuleCommand command, InsertLocation location) {
        CreateObjectFragment fragment = new CreateObjectFragment(location);
        command.add(fragment);
        ObjectRef object = ObjectRef.created(fragment);

        RichText text = RichText.valueOf("<html><body><p>A text with an OLE object:\n<object data=\"sample-word.doc\"/></p></body></html>");
        command.add(new SetAttributeValueFragment(object, "Object Text", text, attachmentResolver));
    }

    private void createObjectWithPicture(EditModuleCommand command, InsertLocation location) {
        Attachment attachment = new AttachmentImpl("sample-picture.png");
        PictureRef picture = PictureRef.create(attachment, attachmentResolver);
        CreatePictureFragment fragment = new CreatePictureFragment(picture, location);
        command.add(fragment);
    }

    private void createTable(EditModuleCommand command, InsertLocation location) {
        int rows = 3;
        int columns = 2;

        // create table
        CreateTableFragment fragment = new CreateTableFragment(rows, columns, location);
        command.add(fragment);
        ObjectRef table = ObjectRef.created(fragment);

        // change text of each cell
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                ObjectRef cell = ObjectRef.cell(table, row, column);
                RichText text = RichText.valueOf("Cell (" + (row + 1) + ", " + (column + 1) + ")");
                command.add(new SetAttributeValueFragment(cell, "Object Text", text));
            }
        }
    }

    private static class AttachmentImpl implements Attachment {

        private final String path;

        private AttachmentImpl(String path) {
            this.path = path;
        }

        @Override
        public String getPath() {
            return path;
        }

        @Override
        public String getMimeType() {
            return null;
        }

        @Override
        public String toString() {
            return path;
        }
    }

    /**
     * Resolves an attachment by loading a local resource using the class loader.
     *
     */
    private static class AttachmentResolverImpl implements AttachmentResolver {

        @Override
        public InputStream getInputStream(Attachment attachment) throws IOException {
            return getClass().getResourceAsStream(attachment.getPath());
        }

        @Override
        public void dispose() {
            // nothing
        }
    }
}
