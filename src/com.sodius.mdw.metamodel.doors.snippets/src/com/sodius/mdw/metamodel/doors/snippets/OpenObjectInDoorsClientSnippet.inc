void openObject(string moduleQualifiedName, int objectAbsoluteNumber) {
	
	// lookup the Item for the given qualified name and ensures it's a Formal Module
	Item i = item(moduleQualifiedName);
	if (i != null && type(i) == "Formal") {
		
		// Open module
		Module m = module(i);
		if (null m) {
			m = read(moduleQualifiedName, true /* display module */, true /* use Standard View */);
			
			// select the Object
			if (m != null) {
				gotoObject(objectAbsoluteNumber, m);
			}
		}
	}
}

