package com.sodius.mdw.metamodel.doors.snippets;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.platform.doors.DoorsApplication;
import com.sodius.mdw.platform.doors.dxl.DXLBufferFragment;
import com.sodius.mdw.platform.doors.dxl.DXLEngine;
import com.sodius.mdw.platform.doors.dxl.DXLFileFragment;
import com.sodius.mdw.platform.doors.dxl.DXLFragment;

/**
 * Opens a module in DOORS active client and navigate to a specific Object.
 * This snippet requires an active DOORS client; a DOORS batch client cannot be used in this scenario.
 *
 * <p>
 * Here are the required Program arguments:
 * <ul>
 * <li><code>args[0]=&lt;moduleQualifiedName&gt;</code> - the qualified name of a DOORS module to open (e.g. "/ParentFolder/MyModule").
 * </li>
 * <li><code>args[1]=&lt;objectAbsoluteNumber&gt;</code> - the absolute number of an Object within the given module (e.g. 34).
 * </li>
 * </ul>
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class OpenObjectInDoorsClientSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            throw new IllegalArgumentException("Expects a module qualified name and object absolute number as arguments");
        }
        String moduleQualifiedName = args[0];
        int objectAbsoluteNumber = Integer.parseInt(args[1]);

        new OpenObjectInDoorsClientSnippet(moduleQualifiedName, objectAbsoluteNumber).call();
    }

    private final String moduleQualifiedName;
    private final int objectAbsoluteNumber;

    private OpenObjectInDoorsClientSnippet(String moduleQualifiedName, int objectAbsoluteNumber) {
        this.moduleQualifiedName = moduleQualifiedName;
        this.objectAbsoluteNumber = objectAbsoluteNumber;
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application) throws Exception {

        // can't execute on a batch client
        if (application.isBatch()) {
            throw new RuntimeException("This snippet requires a DOORS active client");
        }

        // use a DXL file defined along this class
        DXLFragment include = DXLFileFragment.create(OpenObjectInDoorsClientSnippet.class.getResource("OpenObjectInDoorsClientSnippet.inc"));

        // call the appropriate method with the given parameters
        DXLBufferFragment buffer = DXLBufferFragment.create();
        buffer.append("openObject(").escape(moduleQualifiedName).append(", ").append(objectAbsoluteNumber).appendLine(");");

        // execute the DXL code
        System.out.println("Selecting object " + objectAbsoluteNumber + " in module: " + moduleQualifiedName);
        new DXLEngine(application).execute(new DXLFragment[] { include, buffer });
    }

}
