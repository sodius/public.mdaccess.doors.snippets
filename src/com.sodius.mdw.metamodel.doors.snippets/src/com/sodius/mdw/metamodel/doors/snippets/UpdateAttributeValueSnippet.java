package com.sodius.mdw.metamodel.doors.snippets;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.model.RichText;
import com.sodius.mdw.metamodel.doors.io.commands.Command;
import com.sodius.mdw.metamodel.doors.io.commands.CommandRunner;
import com.sodius.mdw.metamodel.doors.io.commands.EditModuleCommand;
import com.sodius.mdw.metamodel.doors.io.commands.EditModuleCommand.TerminationMode;
import com.sodius.mdw.metamodel.doors.io.commands.ModuleRef;
import com.sodius.mdw.metamodel.doors.io.commands.ObjectRef;
import com.sodius.mdw.metamodel.doors.io.commands.SetAttributeValueFragment;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Updates the value of one specific DOORS Object attribute.
 *
 * <p>
 * Here are the required Program arguments:
 * <ul>
 * <li><code>args[0]=&lt;objectURL&gt;</code> - DOORS object URL
 * </li>
 * <li><code>args[1]=&lt;attributeName&gt;</code></li>
 * <li><code>args[2]=&lt;attributeValue&gt;</code></li>
 * </ul>
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class UpdateAttributeValueSnippet extends AbstractCommandSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expects an object URL");
        }
        String url = args[0];

        if (args.length == 1) {
            throw new IllegalArgumentException("Expects an attribute name as argument");
        }
        String attributeName = args[1];

        if (args.length == 2) {
            throw new IllegalArgumentException("Expects an attribute value as argument");
        }
        RichText attributeValue = RichText.valueOf(args[2]);

        new UpdateAttributeValueSnippet(url, attributeName, attributeValue).call();
    }

    private final String url;
    private final String attributeName;
    private final RichText attributeValue;

    public UpdateAttributeValueSnippet(String url, String attributeName, RichText attributeValue) {
        this.url = url;
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application, CommandRunner runner) throws Exception {
        System.out.println("Update attribute '" + attributeName + "' of object " + url);
        runner.run(createCommand());
    }

    private Command createCommand() {

        // edit the module
        ModuleRef module = ModuleRef.url(url);
        EditModuleCommand command = new EditModuleCommand(module, TerminationMode.SAVE_AND_CLOSE);

        // update attribute value
        ObjectRef object = ObjectRef.url(url);
        command.add(new SetAttributeValueFragment(object, attributeName, attributeValue));

        return command;
    }
}
