package com.sodius.mdw.metamodel.doors.snippets;

import java.util.Map;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.doors.Attribute;
import com.sodius.mdw.metamodel.doors.DoorsPackage;
import com.sodius.mdw.metamodel.doors.FormalModule;
import com.sodius.mdw.metamodel.doors.Type;
import com.sodius.mdw.metamodel.doors.connectors.dxl.Common;
import com.sodius.mdw.metamodel.doors.connectors.dxl.OnDemandConfiguration;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Print in the console the content of a DOORS module.
 *
 * <p>
 * Here are the required Program arguments:
 * <ul>
 * <li><code>args[0]=&lt;moduleQualifiedName&gt;</code> - the qualified name of a DOORS module (e.g. "/Folder/MyModule")</li>
 * </ul>
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class PrintModuleContentSnippet extends AbstractReadSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expects a module qualified name as argument");
        }
        String moduleQualifiedName = args[0];
        new PrintModuleContentSnippet(moduleQualifiedName).call();
    }

    private final String moduleQualifiedName;

    public PrintModuleContentSnippet(String moduleQualifiedName) {
        this.moduleQualifiedName = moduleQualifiedName;
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application, Model model) {

        // lookup for the module based on its qualified name
        FormalModule module = resolveFormalModule(model, moduleQualifiedName);

        // print its content
        printContent(module);
    }

    private void printContent(FormalModule module) {
        System.out.println("Reading content of module: " + moduleQualifiedName);
        System.out.println();

        printTypes(module);
        printAttributes(module);
        printObjects(module);
    }

    private void printTypes(FormalModule module) {
        System.out.println(module.getTypes().size() + " types found:");
        for (Type type : module.getTypes()) {
            printType(type);
        }
        System.out.println();
    }

    private void printType(Type type) {
        if (type.isSystem()) {
            System.out.println("- " + type.getName());
        } else {
            // If this is not a system type (i.e. this is a type defined by the module owner),
            // we display its base system type as well.
            System.out.println("- " + type.getName() + " (" + type.getBaseType() + ")");
        }
    }

    private void printAttributes(FormalModule module) {
        System.out.println(module.getAttributes().size() + " attributes found:");
        for (Attribute attribute : module.getAttributes()) {
            printAttribute(attribute);
        }
        System.out.println();
    }

    private void printAttribute(Attribute attribute) {
        System.out.println("- " + attribute.getName() + ": " + attribute.getType());
    }

    private void printObjects(FormalModule module) {
        System.out.println(module.getAllObjects().size() + " objects found:");
        for (com.sodius.mdw.metamodel.doors.Object object : module.getAllObjects()) {
            printObject(object);
        }
    }

    private void printObject(com.sodius.mdw.metamodel.doors.Object object) {

        // An Object might have
        // - an "Object Heading" (object.getObjectHeading())
        // - an "Object Text" (object.getObjectText()),
        // - and/or an "Object Short Text" (object.getObjectShortText())
        // Object.toString() returns the first text slot that is not empty
        // and returns a plain text version (the Object Text might be formatted).
        String text = object.toString();

        System.out.println("- " + object.getObjectNumber() + ": " + text);
    }

    @Override
    protected Map<String, Object> createReaderOptions(DoorsApplication application) {
        Map<String, Object> options = super.createReaderOptions(application);

        // As an example, configure the on-demand loading to ignore anything related to DOORS links
        // (provided they are not useful in this use case)
        // If the module objects contain links to other objects,
        // they will be ignored, even if Object.getOutLinks() is called for example.
        // This might help to optimize a little bit the reading process.
        OnDemandConfiguration configuration = new OnDemandConfiguration.Default();
        configuration.getBlockedTypes().add(DoorsPackage.Literals.EXTERNAL_LINK);
        configuration.getBlockedTypes().add(DoorsPackage.Literals.LINK);
        configuration.getBlockedTypes().add(DoorsPackage.Literals.LINK_MODULE);
        options.put(Common.OPTION_ONDEMAND_CONFIGURATION, configuration);

        return options;
    }
}
