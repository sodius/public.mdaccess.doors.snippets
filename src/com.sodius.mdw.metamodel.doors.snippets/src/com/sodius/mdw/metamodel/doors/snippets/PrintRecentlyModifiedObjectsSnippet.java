package com.sodius.mdw.metamodel.doors.snippets;

import java.util.Calendar;
import java.util.Map;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.doors.DoorsPackage;
import com.sodius.mdw.metamodel.doors.FormalModule;
import com.sodius.mdw.metamodel.doors.connectors.dxl.Common;
import com.sodius.mdw.metamodel.doors.connectors.dxl.Filter;
import com.sodius.mdw.metamodel.doors.connectors.dxl.OnDemandConfiguration;
import com.sodius.mdw.metamodel.doors.connectors.dxl.ReaderScope;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Print in the console the objects of a DOORS module that were modified in the last 2 hours
 *
 * <p>
 * Here are the required Program arguments:
 * <ul>
 * <li><code>args[0]=&lt;moduleQualifiedName&gt;</code> - the qualified name of a DOORS module (e.g. "/Folder/MyModule")</li>
 * </ul>
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class PrintRecentlyModifiedObjectsSnippet extends AbstractReadSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expects a module qualified name as argument");
        }
        String moduleQualifiedName = args[0];
        new PrintRecentlyModifiedObjectsSnippet(moduleQualifiedName).call();
    }

    private final String moduleQualifiedName;

    public PrintRecentlyModifiedObjectsSnippet(String moduleQualifiedName) {
        this.moduleQualifiedName = moduleQualifiedName;
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application, Model model) {

        // lookup for the module based on its qualified name
        FormalModule module = resolveFormalModule(model, moduleQualifiedName);

        // print its content
        printContent(module);
    }

    private void printContent(FormalModule module) {
        System.out.println("Recently modified objects of: " + moduleQualifiedName);

        printObjects(module);
    }

    private void printObjects(FormalModule module) {
        System.out.println(module.getViewObjects().size() + " objects found:");
        for (com.sodius.mdw.metamodel.doors.Object object : module.getViewObjects()) {
            printObject(object);
        }
    }

    private void printObject(com.sodius.mdw.metamodel.doors.Object object) {

        // An Object might have
        // - an "Object Heading" (object.getObjectHeading())
        // - an "Object Text" (object.getObjectText()),
        // - and/or an "Object Short Text" (object.getObjectShortText())
        // Object.toString() returns the first text slot that is not empty
        // and returns a plain text version (the Object Text might be formatted).
        String text = object.toString();

        System.out.println("- " + object.getObjectNumber() + ": " + text);
    }

    @Override
    protected Map<String, Object> createReaderOptions(DoorsApplication application) {
        Map<String, Object> options = super.createReaderOptions(application);

        // Configure the on-demand loading to ignore anything related to DOORS links,
        // as they are not useful in this use case.
        OnDemandConfiguration configuration = new OnDemandConfiguration.Default();
        configuration.getBlockedTypes().add(DoorsPackage.Literals.EXTERNAL_LINK);
        configuration.getBlockedTypes().add(DoorsPackage.Literals.LINK);
        configuration.getBlockedTypes().add(DoorsPackage.Literals.LINK_MODULE);
        options.put(Common.OPTION_ONDEMAND_CONFIGURATION, configuration);

        // set dynamic filtering to retain only objects modified in the last 2 hours
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -2);
        Filter filter = Filter.builder().modifiedAfter(calendar.getTime()).build();
        ReaderScope scope = new ReaderScope();
        scope.getItemScope(moduleQualifiedName).setFilter(filter);
        options.put(Common.OPTION_SCOPE, scope);

        return options;
    }

}
