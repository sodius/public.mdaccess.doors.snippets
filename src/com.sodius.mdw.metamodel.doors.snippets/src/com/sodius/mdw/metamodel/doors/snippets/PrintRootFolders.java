package com.sodius.mdw.metamodel.doors.snippets;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.doors.AggregatedElement;
import com.sodius.mdw.metamodel.doors.DoorsPackage;
import com.sodius.mdw.metamodel.doors.Folder;
import com.sodius.mdw.metamodel.doors.Item;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Print in the console the root folders of a DOORS database.
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class PrintRootFolders extends AbstractReadSnippet {

    public static void main(String[] args) throws Exception {
        new PrintRootFolders().call();
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application, Model model) {

        // get database
        Folder database = model.<Folder> getInstances(DoorsPackage.Literals.FOLDER).first();

        // print folders
        System.out.println("Items:");
        printItems(database, "", 1 /* print only root items */);
    }

    private void printItems(AggregatedElement parent, String prefix, int remainingDepth) {

        // no need to go deeper in the hierarchy?
        if (remainingDepth <= 0) {
            return;
        }

        // loop on items of the given parent
        for (Item item : parent.getOwnedItems()) {

            // print the item (can be a folder or module)
            System.out.println(prefix + "- " + item.getName() + " (" + item.eClass().getName() + ")");

            // if it's a folder, (possibly) print child items
            if (item instanceof AggregatedElement) {
                printItems((AggregatedElement) item, prefix + "  ", remainingDepth - 1);
            }
        }
    }
}
