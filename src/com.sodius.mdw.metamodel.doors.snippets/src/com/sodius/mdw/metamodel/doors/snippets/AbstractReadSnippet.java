package com.sodius.mdw.metamodel.doors.snippets;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import com.sodius.mdw.core.CoreException;
import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.doors.DoorsPackage;
import com.sodius.mdw.metamodel.doors.Folder;
import com.sodius.mdw.metamodel.doors.FormalModule;
import com.sodius.mdw.metamodel.doors.connectors.dxl.Common;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Base snippet to use for reading information out of a DOORS application.
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public abstract class AbstractReadSnippet extends AbstractSnippet {

    @Override
    protected final void run(MDWorkbench workbench, DoorsApplication application) throws Exception {
        Model model = null;
        try {
            model = readDoorsModel(workbench, application);
            run(workbench, application, model);
        } finally {
            if (model != null) {
                model.clear();
            }
        }
    }

    /**
     * Let subclasses defies what is to be done with the provided DOORS model.
     */
    protected abstract void run(MDWorkbench workbench, DoorsApplication application, Model model);

    /**
     * Creates an EMF representation of DOORS data.
     * By default, the EMF model will contain only the DOORS database root folder instance.
     * Information is read from DOORS on demand, when requested using getters on model instances.
     */
    private Model readDoorsModel(MDWorkbench workbench, DoorsApplication application) throws CoreException {
        Model model = workbench.getMetamodelManager().getMetamodel(DoorsPackage.eINSTANCE).createModel();
        model.read("Application", null, createReaderOptions(application));
        return model;
    }

    /**
     * Options to tweak the connection to the DOORS database
     */
    protected Map<String, Object> createReaderOptions(DoorsApplication application) {
        Map<String, Object> options = new HashMap<>();

        // Avoid displaying a UI to configure settings for the connector
        options.put(Common.OPTION_IGNORE_CONNECTOR_UI, true);

        // Request to use the provided DOORS application (otherwise would force using an active DOORS client)
        options.put(Common.OPTION_DOORS_APPLICATION, application);

        return options;
    }

    /**
     * Get the instance representing the DOORS root database.
     * This root folder is guaranteed to be available in the Model.
     * It is the first Folder instance registered in the Model.
     * There might be additional Folder instances later, as Folders get discovered when navigating in DOORS data.
     */
    protected final Folder getRootFolder(Model model) {
        return model.<Folder> getInstances(DoorsPackage.Literals.FOLDER).first();
    }

    /**
     * Request access to a DOORS element given a URI fragment.
     * If this element is not already read at this time,
     * the on-demand process reconnects to DOORS to retrieve the necessary data.
     */
    private final EObject resolveElement(Model model, String uriFragment) {

        // get the EMF resource used to resolve element given they id (here the qualified name)
        Folder rootFolder = getRootFolder(model);
        Resource resource = rootFolder.eResource();

        // resolve the element
        return resource.getEObject(uriFragment);
    }

    /**
     * Request access to a formal module given its qualified name.
     * If this module is not already read at this time,
     * the on-demand process reconnects to DOORS to retrieve the necessary data.
     */
    protected final FormalModule resolveFormalModule(Model model, String qualifiedName) {

        // resolve the module
        EObject object = resolveElement(model, qualifiedName);

        // check the type of the resolved object, the qualified name might refer to a folder for example
        if (object instanceof FormalModule) {
            return (FormalModule) object;
        } else {
            throw new IllegalArgumentException("The qualified name does not resolve to a formal module: " + qualifiedName);
        }
    }

}
