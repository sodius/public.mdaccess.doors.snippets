package com.sodius.mdw.metamodel.doors.snippets;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.metamodel.doors.io.commands.Command;
import com.sodius.mdw.metamodel.doors.io.commands.CommandRunner;
import com.sodius.mdw.metamodel.doors.io.commands.CreateFolderCommand;
import com.sodius.mdw.metamodel.doors.io.commands.FolderRef;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Creates a new DOORS folder.
 *
 * <p>
 * Here are the required Program arguments:
 * <ul>
 * <li><code>args[0]=&lt;folderQualifiedName&gt;</code> - the qualified name of a DOORS folder to create (e.g. "/ParentFolder/MyFolder").
 * The parent folder must already exist
 * </li>
 * </ul>
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public class CreateFolderSnippet extends AbstractCommandSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expects a folder qualified name as argument");
        }
        String folderQualifiedName = args[0];

        new CreateFolderSnippet(folderQualifiedName).call();
    }

    private final String folderQualifiedName;

    public CreateFolderSnippet(String folderQualifiedName) {
        this.folderQualifiedName = folderQualifiedName;
    }

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application, CommandRunner runner) throws Exception {
        System.out.println("Creating folder: " + folderQualifiedName);
        runner.run(createCommand());
    }

    private Command createCommand() {
        FolderRef folder = FolderRef.qualifiedName(folderQualifiedName);
        CreateFolderCommand command = new CreateFolderCommand(folder);
        return command;
    }
}
