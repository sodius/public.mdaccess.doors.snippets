package com.sodius.mdw.metamodel.doors.snippets;

import java.util.concurrent.Callable;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.MDWorkbenchFactory;
import com.sodius.mdw.core.util.DefaultPropertySet;
import com.sodius.mdw.core.util.PropertySet;
import com.sodius.mdw.metamodel.doors.connectors.dxl.Common;
import com.sodius.mdw.metamodel.doors.connectors.dxl.DoorsUtils;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Base code to execute a snippet demonstrating MDAccess for DOORS features.
 *
 * <p>
 * MDAccess for DOORS uses a DoorsApplication instance to connect to DOORS and perform operations.
 * A DoorsApplication instance is what is used to execute DXL code to retrieve information.
 *
 * <p>
 * If the System property "doors.isSilent" is not set or is false, an interactive application is going to be used.
 * This mean the Java program will connect to the active DOORS client through COM automation.
 * The execution will fail if a DOORS client is not running at that time.
 *
 * <p>
 * If the System property "doors.isSilent" is set to true, a batch application is going to be used.
 * This means the Java program will trigger to startup of a DOORS client running in batch mode
 * and will use it for the lifetime of the program execution.
 * The DOORS batch client is automatically shutdown when DoorsApplication.dispose() is called.
 * If batch mode is requested, there are additional System properties that need to be configured.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * <ul>
 * <li><code>-Dmdw.license=&lt;path&gt;</code> -
 * The location of the MDAccess for DOORS license file.
 * Either the path of the license file in your file system or the address of the floating server (e.g. <code>@myserver</code>).
 * </li>
 * </ul>
 * <p>
 * If a DOORS batch client is to use, here are additional required System properties:
 * <ul>
 * <li><code>-Ddoors.path="C:\Program Files\IBM\Rational\DOORS\9.6\bin\doors.exe"</code></li>
 * <li><code>-Ddoors.portserver=36677@myHost</code></li>
 * <li><code>-Ddoors.user=myUser</code></li>
 * <li><code>-Ddoors.password=myPassword</code></li>
 * </ul>
 */
public abstract class AbstractSnippet implements Callable<Void> {

    @Override
    public Void call() throws Exception {
        MDWorkbench workbench = null;
        DoorsApplication application = null;
        try {
            application = createDoorsApplication();
            workbench = MDWorkbenchFactory.create();

            run(workbench, application);
            return null;
        } finally {
            // releases the connection to DOORS and MDWorkbench license
            if (application != null) {
                application.dispose();
            }
            if (workbench != null) {
                workbench.shutdown();
            }
        }
    }

    /**
     * Let subclasses define what is to be done with the provided DoorsApplication
     */
    protected abstract void run(MDWorkbench workbench, DoorsApplication application) throws Exception;

    private DoorsApplication createDoorsApplication() throws Exception {

        // batch mode?
        if (Boolean.getBoolean(Common.PREFERENCE_DOORS_IS_SILENT)) {
            System.out.println("Starting a DOORS batch client...");

            PropertySet properties = new DefaultPropertySet();
            properties.setProperty(Common.PREFERENCE_DOORS_IS_SILENT, true);
            properties.setProperty(Common.PREFERENCE_DOORS_PATH, System.getProperty(Common.PREFERENCE_DOORS_PATH));
            properties.setProperty(Common.PREFERENCE_DOORS_PORTSERVER, System.getProperty(Common.PREFERENCE_DOORS_PORTSERVER));
            properties.setProperty(Common.PREFERENCE_DOORS_USER, System.getProperty(Common.PREFERENCE_DOORS_USER));
            properties.setProperty(Common.PREFERENCE_DOORS_PASSWORD, System.getProperty(Common.PREFERENCE_DOORS_PASSWORD));
            return DoorsUtils.createApplication(properties);
        }

        // interactive mode
        else {
            System.out.println("Connecting to an active DOORS client...");
            return new DoorsApplication();
        }
    }

}
