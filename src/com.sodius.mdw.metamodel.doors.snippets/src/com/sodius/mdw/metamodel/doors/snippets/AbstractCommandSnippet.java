package com.sodius.mdw.metamodel.doors.snippets;

import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.metamodel.doors.io.commands.CommandRunner;
import com.sodius.mdw.platform.doors.DoorsApplication;

/**
 * Base snippet to use for creating and updating information in a DOORS application.
 *
 * <p>
 * See AbstractSnippet Javadoc for information on how to run a snippet and the required System properties to configure.
 *
 * @see AbstractSnippet
 */
public abstract class AbstractCommandSnippet extends AbstractSnippet {

    @Override
    protected void run(MDWorkbench workbench, DoorsApplication application) throws Exception {
        CommandRunner runner = new CommandRunner(workbench, application);
        run(workbench, application, runner);
    }

    protected abstract void run(MDWorkbench workbench, DoorsApplication application, CommandRunner runner) throws Exception;

}
